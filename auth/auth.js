const bcrypt = require("bcrypt-nodejs");
const knex = require("knex");
const jwtGenerator = require("../utils/jwtGenerator");
const jwt = require("jsonwebtoken");

const db = knex({
  client: "pg",
  connection: {
    host: "127.0.0.1",
    user: "postgres",
    password: "megathrone",
    database: "legaldiary",
  },
});

// const verifyToken = (req,res,next)=>{
//        const bearerHeader = req.headers['authorization'];
//        if(typeof bearerHeader !== 'undefined'){
//          //split at space
//          const bearer = bearerHeader.split(' ');
//          // Get token from array
//          const bearerToken = bearer[1];
//          //set the token
//          req.token= bearerToken
//          // next middleware
//          console.log(req.token);
//          next();
//       } else{
//         res.sendStatus(403);
//       }
//     }

const postLogin = (req, res) => {
  db.select("email", "hash")
    .from("login")
    .where("email", "=", req.body.email)
    .then((data) => {
      // console.log(data[0].hash)
      const isValid = bcrypt.compareSync(req.body.password, data[0].hash);
      console.log(isValid);
      if (isValid) {
        return db
          .select("*")
          .from("expert")
          .where("email", "=", req.body.email)
          .then((expert) => {
            jwt.sign(
              { expert },
              "secretKey",
              { expiresIn: "1hr" },
              (err, token) => {
                var user = {
                  token: token,
                  expert: expert,
                };
                res.json({ user });
              }
            );
          })
          .catch((err) =>
            res.status(400).json("Username and Password Incorrect")
          );
      } else {
        res.status(400).json("Password or Email Incorrect");
      }
    })
    .catch((err) => res.status(500).json("Server Error"));
};

// This Function is working, but  I still have doubts.

const postUser = (req, res) => {
  const {
    first_name,
    middle_name,
    last_name,
    username,
    password,
    role,
    date_of_birth,
    gender,
    email,
    account,
    created_on,
    phone,
  } = req.body;
  const hash = bcrypt.hashSync(password);
  db.transaction((trx) => {
    trx
      .insert({
        email: email,
        username: username,
        account: account,
        hash: hash,
      })
      .into("login")
      .returning("*")
      .then((data) => {
        return trx("expert")
          .insert({
            first_name: first_name,
            middle_name: middle_name,
            last_name: last_name,
            username: username,
            email: email,
            date_of_birth: date_of_birth,
            gender: gender,
            phone: phone,
            role: role,
            account: account,
            created_on: created_on,
          })
          .then(() => {
            //  let login_id = data[0].login_id
            jwt.sign(
              { data },
              "secretKey",
              { expiresIn: "1hr" },
              (err, token) => {
                var user = {
                  token: token,
                  data: data,
                };
                res.json({ user });
              }
            );
          });
      })
      .then(trx.commit)
      .catch(trx.rollback);
  }).catch((err) => res.status(500).json({ dbError: "Unable to register" }));
};

module.exports = {
  postUser,
  postLogin,
};
