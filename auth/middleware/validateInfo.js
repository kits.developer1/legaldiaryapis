module.exports = function (req, res, next) {
  const {
    first_name,
    middle_name,
    last_name,
    username,
    role,
    password,
    date_of_birth,
    gender,
    email,
    phone,
    account,
  } = req.body;

  function validEmail(userEmail) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(userEmail);
  }

  if (req.path === "/register") {
    if (
      [
        first_name,
        middle_name,
        last_name,
        username,
        role,
        password,
        date_of_birth,
        gender,
        email,
        phone,
        account,
      ].every(Boolean)
    ) {
      return res.json("Missing Credentials");
    } else if (!validEmail(email)) {
      return res.json("Invalid Email");
    }
  } else if (req.path === "/signin") {
    if (![email, password].every(Boolean)) {
      return res.json("Missing Credentials");
    } else if (!validEmail(email)) {
      return res.json("Invalid Email");
    }
  }

  next();
};
