//const knexfile = require("../knexfile");
//require("dotenv").config();
// const dbEngine = process.env.DB_ENV || "development";
// const config = require("../knexfile")[dbEngine];

// module.exports = require("knex")(config);

const knex = require("knex");
const knexfile = require("../knexfile");

const enviroment = process.env.NODE_ENV || "development";

module.exports = knex(knexfile[enviroment]);
