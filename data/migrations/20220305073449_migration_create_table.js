exports.up = function (knex, Promise) {
  return knex.schema
     .createTable('accounts', function(table) {
            table.increments("account_id").primary()
            table.string('account_name', 255).notNullable()
            table.string('email', 255).unique().notNullable()
            table.string('phone', 255).unique().notNullable()
            table.string('law_firm', 255).unique().notNullable()
            table.boolean('account_type', 255).notNullable().defaultTo(true)
            table.boolean('validated', 255).notNullable().defaultTo(false)
            table.boolean('is_delete', 255).notNullable().defaultTo(false)
            table.timestamp('created_on').notNullable().defaultTo(knex.fn.now())
            table.timestamp('deleted_at').defaultTo(knex.fn.now())
            table.timestamp('updated_at').defaultTo(knex.fn.now())
        })
    .createTable("expert", function (table) {
      table.increments("expert_id").primary();
      table.string("first_name", 255).notNullable();
      table.string("middle_name", 255).notNullable();
      table.string("last_name", 255).notNullable();
      table.date("date_of_birth", 255).notNullable();
      table.string("gender", 255).notNullable();
      table.string("phone", 255).notNullable().unique();
      table.string("role", 255).notNullable().defaultTo("admin");
      table.string("username", 255).notNullable().unique();
      table.string("email", 255).notNullable().unique();
      table.boolean("is_delete").notNullable().defaultTo(false);
      table.timestamp("created_on").defaultTo(knex.fn.now());
      table
        .integer("account")
        .references("account_id")
        .inTable("accounts")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
    })
    .createTable("login", function (table) {
      table.increments("login_id").primary();
      table.string("username", 255).notNullable().unique();
      table.string("email", 255).notNullable().unique();
      table.string("hash", 255).notNullable().unique();
      table
        .integer("account")
        .references("account_id")
        .inTable("accounts")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table.timestamp("created_on").defaultTo(knex.fn.now());
    })
    .createTable("client", function (table) {
      table.increments("client_id").primary();
      table.string("first_name", 255).notNullable();
      table.string("middle_name", 255).notNullable();
      table.string("last_name", 255).notNullable();
      table.date("date_of_birth", 255).notNullable();
      table.string("gender", 255).notNullable();
      table.string("other_details", 255);
      table.string("company", 255);
      table.string("title", 255);
      table.string("country", 255);
      table.string("fax", 255);
      table.string("city", 255);
      table.string("address", 255);
      table.string("postal_code", 255);
      table.string("phone", 255).notNullable().unique();
      table.string("username", 255).notNullable().unique();
      table.string("email", 255).notNullable().unique();
      table.string("id_card_number", 255);
      table.boolean("is_delete").notNullable().defaultTo(false);
      table.timestamp("created_on").defaultTo(knex.fn.now());
      table
        .integer("account")
        .references("account_id")
        .inTable("accounts")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
    })
    .createTable("case_category", function (table) {
      table.increments("case_category_id").primary();
      table.string("category_name", 255).notNullable();
      table.boolean("is_delete").notNullable().defaultTo(false);
      table
        .integer("account")
        .references("account_id")
        .inTable("accounts")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table.timestamp("created_on").defaultTo(knex.fn.now());
    })
    .createTable("case_details", function (table) {
      table.increments("case_details_id").primary();
      table.string("case_name", 255).notNullable();
      table.string("case_ref_number", 255).notNullable().unique();
      table.string("case_description", 255).notNullable();
      table.date("open_date", 255);
      table.date("close_date", 255);
      table.date("ajourned_date", 255);
      table.string("non_client", 255).notNullable();
      table.string("court", 255).notNullable();
      table.string("judge", 255);
      table.string("court_type", 255);
      table.string("case_observation", 255);
      table.string("court_registrar", 255);
      table.string("case_fee", 255);
      table.string("location", 255);
      table.string("remark", 255);
      table.boolean("is_delete").notNullable().defaultTo(false);
      table.timestamp("created_on").defaultTo(knex.fn.now());
      table
        .integer("account")
        .references("account_id")
        .inTable("accounts")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("expert")
        .references("expert_id")
        .inTable("expert")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("client")
        .references("client_id")
        .inTable("client")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("category")
        .references("case_category_id")
        .inTable("case_category")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
    })
    .createTable("settlement_cases", function (table) {
      table.increments("settle_case_id").primary();
      table.string("settle_case_name", 255).notNullable();
      table.string("ref_number", 255).notNullable().unique();
      table.string("non_client_id_number", 255).notNullable().unique();
      table.string("non_client", 255).notNullable();
      table.string("case_fee", 255);
      table.string("location", 255);
      table.string("status", 255);
      table.string("settle_terms", 255);
      table.date("settle_date");
      table.boolean("is_delete").notNullable().defaultTo(false);
      table.timestamp("created_on").defaultTo(knex.fn.now());
      table
        .integer("account")
        .references("account_id")
        .inTable("accounts")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("expert")
        .references("expert_id")
        .inTable("expert")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("client")
        .references("client_id")
        .inTable("client")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("category")
        .references("case_category_id")
        .inTable("case_category")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
    })
    .createTable("tasks", function (table) {
      table.increments("task_id").primary();
      table.string("subject", 255).notNullable().unique();
      table.date("due_date", 255);
      table.boolean("status").defaultTo(false);
      table.string("description", 255).notNullable().unique();
      table.boolean("repeat").defaultTo(false);
      table
        .integer("expert")
        .references("expert_id")
        .inTable("expert")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("client")
        .references("client_id")
        .inTable("client")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table
        .integer("case_details")
        .references("case_details_id")
        .inTable("case_details")
        .onDelete("CASCADE")
        .onUpdate("CASCADE");
      table.timestamp("created_on").defaultTo(knex.fn.now());
    });
};

exports.down = function (knex) {
  return knex.schema
    .dropTable("accounts")
    .dropTable("expert")
    .dropTable("login")
    .dropTable("client")
    .dropTable("case_category")
    .dropTable("case_details")
    .dropTable("settlement_cases")
    .dropTable("tasks")
    .dropTable("company");
};
