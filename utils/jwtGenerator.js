const jwt = require('jsonwebtoken');
require("dotenv").config();

   jwtGenerator=(login_id)=>{
       const payload = {
           login: login_id
       }
       
       return jwt.sign({payload}, process.env.jwtSecret, {expiresIn: "30 days"});
   };

   module.exports = jwtGenerator;
