const express = require("express");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const fileupload = require("express-fileupload");

const app = express();

// ==> API Routes:
const test = require("./routes/test.routes");
const accountRoute = require("./routes/accountsExpert.routes");
const authRoute = require("./routes/authentication.routes");
const caseRoute = require("./routes/case.routes");
const caseCategoryRoute = require("./routes/caseCategory.routes");
const clientRoute = require("./routes/clients.routes");
const companyRoute = require("./routes/companies.routes");
const expertRoute = require("./routes/expert.routes");
const fileRoute = require("./routes/file.routes");
const settlementRoute = require("./routes/settlementCase.routes");
const taskRoute = require("./routes/tasks.routes");
const permissionRolesRoute = require("./routes/permissionsRoles.routes");

// ==> Needed to be able to read body data
app.use(express.urlencoded({ extended: true })); // support URL-encoded bodies
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.json({ type: "application/vnd.api+json" }));
app.use(cookieParser()); // USE EXPRESS MIDDLEWARE FOR EASIER COOKIE HANDLING
//app.use(cors());
app.use(
  cors({
    origin: "http://localhost:3000",
    methods: ["POST", "PUT", "GET", "OPTIONS", "HEAD"],
    credentials: true,
  })
);
app.use(fileupload());
app.use("./uploads", express.static(__dirname + "./uploads"));

app.use(test);
app.use("/api/v1/", accountRoute);
app.use("/api/v1/", permissionRolesRoute);
app.use("/api/v1/", authRoute);
app.use("/api/v1/", caseRoute);
app.use("/api/v1/", caseCategoryRoute);
app.use("/api/v1/", clientRoute);
app.use("/api/v1/", companyRoute);
app.use("/api/v1/", expertRoute);
app.use("/api/v1/", fileRoute);
app.use("/api/v1/", settlementRoute);
app.use("/api/v1/", taskRoute);

module.exports = app;
