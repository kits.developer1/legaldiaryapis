const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
  //console.log(req.headers);
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    //split at space
    const bearer = bearerHeader.split(" ");
    console.log(bearer);
    // Get token from array
    const bearerToken = bearer[1];

    try {
      const decoded = jwt.verify(bearerToken, process.env.ACCESS_TOKEN_SECRET);
      console.log("MIDDLEWARE DECODED!!!!");
      console.log(decoded);
      //set the token
      req.user = decoded;
      // next middleware
      console.log("MIDDLEWARE!!!!");
      console.log(req.user);
      next();
    } catch (error) {
      console.log("erorr " + error);
      return res.status(401).json({ error: error });
    }
  } else {
    return res.status(401).json({ error: "Access denied" });
  }
};
