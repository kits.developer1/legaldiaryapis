const bcrypt = require("bcrypt-nodejs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const crypto = require("crypto");

const {
  createAccessToken,
  createRefreshToken,
  sendAccessToken,
  sendRefreshToken,
} = require("../helperFuctions/tokens");

require("dotenv").config();

const db = require("../../../data/db");

const transporter = nodemailer.createTransport({
  host: "mail.kutoracrm.com",
  port: 465, // Secure SMTP port
  secure: true, // Use SSL
  auth: {
    user: "monyuy@kutoracrm.com",
    pass: "w6akjI&$0t)~",
  },
});

// Function to generate a verification token
function generateVerificationToken() {
  // Implement token generation logic here
  return crypto.randomBytes(32).toString("hex");
}

// Hash the verification token
function hashToken(token) {
  return crypto.createHash("sha256").update(token).digest("hex");
}

// Function to send verification email
async function sendVerificationEmail(email, verificationToken) {
  // Implement your email sending logic here
  // This could involve using a library like nodemailer to send an email with a verification link containing the token
  const mailOptions = {
    from: "monyuy@kutoracrm.com",
    to: email,
    subject: "Email Verification",
    html: `
      <p>Please click the following link to verify your email:</p>
      <a href="http://yourapp.com/verify/${verificationToken}">Verify Email</a>
    `,
  };

  try {
    await transporter.sendMail(mailOptions);
    console.log("Verification email sent");
  } catch (error) {
    console.error("Error sending verification email:", error);
  }
}

// This Function works fine, but  I still have doubts.

const postUser = (req, res) => {
  console.log(req.body);
  const {
    account_name,
    first_name,
    last_name,
    password,
    address,
    gender,
    email,
    phone,
    law_firm,
    created_on,
  } = req.body;
  // ==> check if user exist
  db.select("email")
    .from("accounts")
    .where("email", "=", req.body.email)
    .then((data) => {
      if (data && data.length > 0) {
        res.status(403).json({ USER: "USER ALREADY EXIST" });
      } else {
        // ==> IF USER DONT EXIST HASH PASSWORD
        const verificationToken = generateVerificationToken();
        const hashedToken = hashToken(verificationToken);
        const hash = bcrypt.hashSync(password);
        db.transaction((trx) => {
          return trx("accounts")
            .insert({
              account_name: account_name,
              email: email,
              phone: phone,
              law_firm: law_firm,
              verification_token: hashedToken,
              created_on: created_on,
            })
            .returning("*")
            .then((data) => {
              console.log(data);
              // RETURNING ALL USE ACCOUNT ID  AND INSERT IN EXPERT
              sendVerificationEmail(email, verificationToken);
              const account = data[0].account_id;
              return trx("expert")
                .insert({
                  email: email,
                  phone: phone,
                  hash: hash,
                  first_name: first_name,
                  last_name: last_name,
                  address: address,
                  gender: gender,
                  created_on: created_on,
                  account_id: account,
                  law_firm: law_firm,
                })
                .returning("*")
                .then((data) => {
                  const accountId = data[0].account_id;
                  const expertId = data[0].expert_id;
                  const role_id = 1;
                  return trx("expert_roles")
                    .insert({
                      expert_id: expertId,
                      role_id: role_id,
                      account_id: accountId,
                      created_on: created_on,
                    })
                    .returning("*")
                    .then((data) => {
                      res.status(200).send({ MESSAGE: "ACCOUNT CREATED" });
                    });
                });
            })
            .then(trx.commit)
            .catch(trx.rollback);
        });
      }
    })
    .catch((err) => res.status(500).json({ dbError: err }));
};

const postLogin = (req, res) => {
  console.log(req.body);
  db.select("email", "hash")
    .from("expert")
    .where("email", "=", req.body.email)
    .then((data) => {
      console.log("DATA");
      console.log(data[0].hash);
      const isValid = bcrypt.compareSync(req.body.password, data[0].hash);
      console.log("ISVALID");
      console.log(isValid);
      if (isValid) {
        return db
          .select("email")
          .from("expert")
          .where("email", "=", data[0].email)
          .where("is_delete", "=", false)
          .where("is_block", "=", false)
          .then((expertEmail) => {
            console.log("LOGIN!!!");
            console.log(expertEmail[0].email);
            const expert = expertEmail[0].email;
            // CREATE REFRESH AND ACCESS TOKEN
            const accessToken = createAccessToken(expert);
            const refreshToken = createRefreshToken(expert);
            //  ADD REFRESHTOKEN TO DATABASE
            db("expert")
              .update({
                refresh_token: refreshToken,
              })
              .where("email", expertEmail[0].email)
              .returning("*")
              .then((expertData) => {
                const account = expertData[0].account_id;
                console.log("EXPERTDATA!!!");
                console.log(data);
                db("expert_roles")
                  .join("roles", "expert_roles.role_id", "=", "roles.role_id")
                  .select("roles.role_id", "roles.role_name")
                  .where("expert_roles.account_id", account)
                  .then((userRoles) => {
                    console.log("USERROLES!!!");
                    console.log(userRoles);
                    //SEND TOKEN. REFRESH TOKEN AS A COOKIE AND ACCESS TOKEN AS A REGULAR RESPONSE
                    sendRefreshToken(res, refreshToken);
                    sendAccessToken(res, expertData, userRoles, accessToken);
                  });
              });
          })
          .catch((err) => res.status(400).json({ ERROR: err }));
      } else {
        res.status(403).send("Password don't match");
      }
    })
    .catch((err) => res.status(500).json({ "SERVER ERROR": err }));
};

const logout = (req, res) => {
  console.log(req);
  res.clearCookie("refreshToken");
  return res.send({ userInfo: [] });
};

// GET A NEW ACCESS TOKEN WITH A REFRESH TOKEN

const refreshToken = (req, res) => {
  console.log(req.cookies.refreshToken);
  console.log("REFRESH TOKEN REQ");
  let payload = null;
  const token = req.cookies.refreshToken;
  if (!token) {
    return res.status(403).json({ accessToken: null });
  } else {
    try {
      payload = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET);

      console.log("PAYLOAD");
      console.log(payload);
      const Email = payload.expert;
      db.select("*")
        .from("expert")
        .where("email", "=", Email)
        .then((expert) => {
          console.log(expert[0]);
          console.log("WHERE!!");
          console.log(expert);
          // CREATE REFRESH AND ACCESS TOKEN
          const accessToken = createAccessToken(expert[0]?.email || []);
          const refreshToken = createRefreshToken(expert[0]?.email || []);
          //  ADD REFRESHTOKEN TO DATABASE
          db("expert")
            .update({
              refresh_token: refreshToken,
            })
            .where("email", expert[0].email)
            .returning("*")
            .then((expertData) => {
              console.log("UPDATE");
              console.log(expertData);
              const account = expertData[0]?.account_id;
              console.log("EXPERTDATA!!!");
              console.log(expertData);
              db("expert_roles")
                .join("roles", "expert_roles.role_id", "=", "roles.role_id")
                .select("roles.role_id", "roles.role_name")
                .where("expert_roles.account_id", account)
                .then((userRoles) => {
                  console.log("USERROLES!!!");
                  console.log(userRoles);
                  //SEND TOKEN. REFRESH TOKEN AS A COOKIE AND ACCESS TOKEN AS A REGULAR RESPONSE
                  sendRefreshToken(res, refreshToken);
                  sendAccessToken(res, expertData, userRoles, accessToken);
                });
            });
        });
    } catch (err) {
      return res.status(403).send({ accessToken: null });
    }
  }
};

const updateAccount = (req, res) => {
  console.log(req.body);
  const {
    first_name,
    last_name,
    password,
    address,
    gender,
    account,
    email,
    updated_at,
  } = req.body;
  const hash = bcrypt.hashSync(password);
  db("expert")
    .select({ email, account_id })
    .where("account_id", account)
    .then((data) => {
      // check if data exist, if exist update data using email
      if (data) {
        db("expert")
          .update({
            first_name,
            last_name,
            hash,
            address,
            gender,
            updated_at,
          })
          .where("email", email)
          .returning("*")
          .then((data) => {
            res.json(data);
          })
          .catch((err) => res.status(400).json({ dbError: "db error" }));
      } else {
        res.json("data not found");
      }
    })
    .catch((err) => res.status(400).json({ dbError: "bad request" }));
};

module.exports = {
  postUser,
  postLogin,
  logout,
  refreshToken,
  updateAccount,
};
