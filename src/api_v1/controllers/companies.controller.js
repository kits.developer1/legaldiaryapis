const db = require("../../../data/db");
require("dotenv").config();

const getCompany = (req, res) => {
  const { account } = req.body;
  db.select("*")
    .from("company")
    .where("account", account)
    .where("is_delete", false)
    .then((data) => {
      res.json(data);
    })
    .catch((err) =>
      res.status(400).json({
        dbError: "db error",
      })
    );
};

const postCompany = (req, res) => {
  const {
    company_name,
    reg_number,
    address,
    phone,
    fax,
    other_details,
    country,
    city,
    postal_code,
    account,
    created_on,
  } = req.body;
  db("company")
    .insert({
      company_name,
      reg_number,
      address,
      phone,
      fax,
      other_details,
      country,
      city,
      postal_code,
      account,
      created_on,
    })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putCompany = (req, res) => {
  const {
    company_id,
    company_name,
    reg_number,
    address,
    phone,
    fax,
    other_details,
    country,
    city,
    postal_code,
  } = req.body;
  db("company")
    .select({ company_id })
    .where(`company_id`, company_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("company")
          .update({
            company_name,
            reg_number,
            address,
            phone,
            fax,
            other_details,
            country,
            city,
            postal_code,
          })
          .where("company_id", company_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const deleteCompany = (req, res) => {
  const { company_id } = req.body;
  db("company")
    .select({ company_id })
    .where("company_id", company_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        console.log(data.company_id);
        db("company")
          .update({ is_delete: true })
          .where("company_id", company_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getCompany,
  postCompany,
  putCompany,
  deleteCompany,
};
