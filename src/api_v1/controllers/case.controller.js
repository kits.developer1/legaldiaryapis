const db = require("../../../data/db");
require("dotenv").config();

const getCase = (req, res) => {
  const { account } = req.params;
  db("case_details")
    .join("expert", "case_details.expert", "=", "expert.expert_id")
    .join("client", "case_details.client", "=", "client.client_id")
    .join(
      "case_category",
      "case_details.category",
      "=",
      "case_category.case_category_id"
    )
    .select(
      "case_details.case_details_id",
      "case_details.category",
      "case_details.client",
      "case_details.expert",
      "case_details.case_ref_number",
      "case_details.location",
      "case_details.case_name",
      "case_details.is_delete",
      "case_details.case_fee",
      "case_details.non_client",
      "case_details.court",
      "case_details.ajourned_date",
      "case_details.judge",
      "case_details.court_type",
      "case_details.court_registrar",
      "case_details.open_date",
      "case_details.close_date",
      "case_details.case_description",
      "case_details.status",
      "case_details.case_observation",
      "case_details.remark",
      "case_details.created_on",
      "expert.email",
      "client.id_card_number",
      "case_category.name"
    )
    .where("account", account)
    .where("is_delete", false)
    .then((data) => {
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).json({ INFO: "NO DATA FOUND" });
      }
    })
    .catch((err) =>
      res.status(400).json({
        dbError: "bad request",
      })
    );
};

const postCase = (req, res) => {
  const { account } = req.params;
  const {
    case_name,
    case_ref_number,
    case_description,
    status,
    open_date,
    close_date,
    ajourned_date,
    non_client,
    court,
    judge,
    court_type,
    case_observation,
    court_registrar,
    case_fee,
    location,
    category,
    client,
    expert,
    remark,
    created_on,
  } = req.body;
  db("case_details")
    .insert({
      account,
      case_name,
      case_ref_number,
      case_description,
      status,
      open_date,
      close_date,
      ajourned_date,
      non_client,
      case_observation,
      court_registrar,
      court_type,
      judge,
      court,
      case_fee,
      location,
      category,
      client,
      expert,
      remark,
      account,
      created_on,
    })
    .returning("*")
    .then((data) => {
      res.json(data);
    })

    .catch((err) => res.status(500).json({ dbError: "bad request" }));
};

const putCase = (req, res) => {
  const {
    account,
    case_details_id,
    case_name,
    case_ref_number,
    case_description,
    status,
    open_date,
    close_date,
    ajourned_date,
    non_client,
    court,
    judge,
    court_type,
    case_observation,
    court_registrar,
    case_fee,
    location,
    category,
    client,
    expert,
    remark,
  } = req.body;
  db("case_details")
    .select({ case_details_id })
    .where(`account`, account)
    .where(`case_details_id`, case_details_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("case_details")
          .update({
            case_name,
            case_ref_number,
            case_description,
            status,
            open_date,
            close_date,
            ajourned_date,
            non_client,
            court,
            judge,
            court_type,
            case_observation,
            court_registrar,
            case_fee,
            location,
            category,
            client,
            expert,
            remark,
          })
          .where("case_details_id", case_details_id)
          .returning("*")
          .then((data) => {
            res.json(data);
          })
          .catch((err) => res.status(400).json({ dbError: "db error" }));
      } else {
        res.json("data not found");
      }
    })
    .catch((err) => res.status(400).json({ dbError: "bad request" }));
};

const deleteCase = (req, res) => {
  const { account } = req.params;
  const { case_details_id } = req.body;
  db("case_details")
    .select({ case_details_id })
    .where("account", account)
    .where("case_details_id", case_details_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        db("case_details")
          .update({ is_delete: true })
          .where("case_details_id", case_details_id)
          .returning("*")
          .then((data) => {
            res.json(data);
          })
          .catch((err) => res.status(400).json({ dbError: "bad request" }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getCase,
  postCase,
  putCase,
  deleteCase,
};
