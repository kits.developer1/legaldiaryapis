const db = require("../../../data/db");
require("dotenv").config();

const getClients = (req, res) => {
  const { account_id } = req.params;
  db("clients")
    .select("*")
    .where("account_id", account_id)
    .where("is_delete", false)
    .then((data) => {
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).json({ INFO: "NO DATA FOUND" });
      }
    })
    .catch((err) =>
      res.status(400).json({
        dbError: "db error",
      })
    );
};

// const getAllContacts = (req, res) => {
//   const { account_id } = req.params;
//   db("clients")
//     .select("*")
//     .where("account_id", account_id)
//     .then((data) => {
//       if (data) {
//         res.status(200).json(data);
//       } else {
//         res.status(404).json({ INFO: "NO DATA FOUND" });
//       }
//     })
//     .catch((err) => {
//       res.status(400).json({
//         dbError: err,
//       });
//     });
// };

const postClient = (req, res) => {
  const { account_id } = req.params;
  const {
    first_name,
    last_name,
    email,
    profession,
    employer,
    phone,
    address,
    id_card_number,
    city,
    country,
    other_details,
    created_on,
  } = req.body;
  db("clients")
    .insert({
      first_name,
      last_name,
      email,
      profession,
      employer,
      phone,
      address,
      id_card_number,
      city,
      country,
      other_details,
      account_id,
      created_on,
    })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putClient = (req, res) => {
  console.log(req.body);
  const {
    account_id,
    client_id,
    first_name,
    last_name,
    email,
    profession,
    employer,
    phone,
    address,
    id_card_number,
    city,
    country,
    other_details,
  } = req.body;
  db("clients")
    .select({ client_id })
    .where(`account_id`, account_id)
    .where(`client_id`, client_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("clients")
          .update({
            first_name,
            last_name,
            email,
            profession,
            employer,
            phone,
            address,
            id_card_number,
            city,
            country,
            other_details,
          })
          .where("client_id", client_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const deleteClient = (req, res) => {
  const { client_id } = req.body;
  db("client")
    .select({ client_id })
    .where("client_id", client_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        console.log(data.client_id);
        db("client")
          .update({ is_delete: true })
          .where("client_id", client_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const totalNumberOfClients = (req, res) => {
  const { account_id } = req.params;
  db("clients")
    .sum({ totalNumberOfClients: "client_id" })
    .where("account_id", account_id)
    .then((total) => {
      res.json(total);
    })
    .catch((err) => res.status(400).json({ Error: "bad request" }));
};

module.exports = {
  getClients,
  postClient,
  putClient,
  deleteClient,
  totalNumberOfClients,
};
