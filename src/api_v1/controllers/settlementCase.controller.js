const db = require("../../../data/db");
require("dotenv").config();

const getSettlementCase = (req, res) => {
  const { account } = req.body;
  db("settlement_cases")
    .join("expert", "settlement_cases.expert", "=", "expert.expert_id")
    .join("client", "settlement_cases.client", "=", "client.client_id")
    .select(
      "settlement_cases.settle_case_id",
      "settlement_cases.settle_case_name",
      "settlement_cases.settle_date",
      "settlement_cases.settle_terms",
      "settlement_cases.non_client_id_number",
      "settlement_cases.status",
      "settlement_cases.location",
      "settlement_cases.is_delete",
      "settlement_cases.ref_number",
      "expert.username",
      "client.id_card_number",
      "settlement_cases.client",
      "settlement_cases.expert",
      "settlement_cases.non_client"
    )
    .where("account", account)
    .where("settlement_cases.is_delete", false)
    .then((data) => {
      res.json(data);
    })
    .catch((err) =>
      res.status(400).json({
        dbError: "bad request",
      })
    );
};

const postSettlementCase = (req, res) => {
  const {
    settle_case_name,
    ref_number,
    client,
    expert,
    non_client,
    settle_date,
    settle_terms,
    non_client_id_number,
    status,
    location,
    account,
  } = req.body;
  db("settlement_cases")
    .insert({
      settle_case_name,
      ref_number,
      client,
      expert,
      non_client,
      settle_date,
      settle_terms,
      non_client_id_number,
      status,
      location,
      account,
    })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "bad request" }));
};

const putSettlementCase = (req, res) => {
  const {
    settle_case_id,
    settle_case_name,
    ref_number,
    client,
    expert,
    non_client,
    settle_date,
    settle_terms,
    non_client_id_number,
    status,
    location,
  } = req.body;
  db("settlement_cases")
    .select({ settle_case_id })
    .where(`settle_case_id`, settle_case_id)
    .then((data) => {
      if (data.length > 0) {
        db("settlement_cases")
          .update({
            settle_case_name,
            ref_number,
            client,
            expert,
            non_client,
            settle_date,
            settle_terms,
            non_client_id_number,
            status,
            location,
          })
          .where("settle_case_id", settle_case_id)
          .returning("*")
          .then((data) => {
            res.json(data);
          })
          .catch((err) => res.status(500).json({ dbError: "db error" }));
      } else {
        res.json("data not found");
      }
    })
    .catch((err) => res.status(400).json({ dbError: "bad request" }));
};

const deleteSettlementCase = (req, res) => {
  const {
    settle_case_id,
    settle_case_name,
    client,
    expert,
    non_client,
    settel_date,
    description,
    settle_terms,
    non_client_id_number,
    status,
    location,
  } = req.body;
  console.log(req.body);
  db("settlement_cases")
    .select({ settle_case_id })
    .where("settle_case_id", settle_case_id)
    .then((data) => {
      if (data) {
        db("settlement_cases")
          .update({ is_delete: true })
          .where("settle_case_id", settle_case_id)
          .returning("*")
          .then((data) => {
            res.json(data);
          })
          .catch((err) => res.status(400).json({ dbError: "bad request" }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getSettlementCase,
  postSettlementCase,
  putSettlementCase,
  deleteSettlementCase,
};
