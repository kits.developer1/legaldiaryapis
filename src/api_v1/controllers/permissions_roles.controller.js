const db = require("../../../data/db");
require("dotenv").config();

// ==> Get All roles for a given account.
const getAllRoles = (req, res) => {
  console.log(req.params);
  const { account_id } = req.params;
  console.log(account_id);
  db("roles")
    .select("*")
    .where("account_id", account_id)
    .then((data) => {
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).json({ INFO: "NO DATA FOUND" });
      }
    })
    .catch((err) => {
      res.status(400).json({
        dbError: err,
      });
    });
};

// ==> Get All Permissions for a given account.
const getAllPermissions = (req, res) => {
  console.log(req.params);
  const { account_id } = req.params;
  console.log(account_id);
  db("permissions")
    .select("*")
    .where("account_id", account_id)
    .then((data) => {
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).json({ INFO: "NO DATA FOUND" });
      }
    })
    .catch((err) => {
      res.status(400).json({
        dbError: err,
      });
    });
};

// ==> Add Role for a given account.
const postAddRole = (req, res) => {
  const { account_id } = req.params;
  const { role_name, created_on } = req.body;
  db("roles")
    .insert({ role_name, account_id, created_on })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: err }));
};

// ==> Add Permission for a given account.
const postAddPermission = (req, res) => {
  console.log(req.body);
  const { account_id } = req.params;
  const { permission, description, created_on } = req.body;
  db("permissions")
    .insert({ permission, description, account_id, created_on })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

// Assign role to expert
// ===> Bug prevent ASSINGING THESAME ROLE TWICE TO SAME EXPERT.
const assignRoleToExpert = (req, res) => {
  const { account_id } = req.params;
  const { expert_id, role_id, created_on } = req.body;
  // CHECK IF ROLE ALREADY EXIST FOR THIS EXPERT
  db.select("*")
    .from("expert_roles")
    .where("account_id", "=", account_id)
    .where("expert_id", "=", req.body.expert_id)
    .where("role_id", "=", req.body.role_id)
    .then((data) => {
      console.log(data);
      if (data && data.length > 0) {
        res.status(403).json({ INFO: "ROLE ALREADY EXIST FOR THIS EXPERT" });
      } else {
        // ===> IF NOT PROCEED TO ASSIGN ROLE
        db("expert_roles")
          .insert({ expert_id, role_id, account_id, created_on })
          .returning("*")
          .then((data) => {
            res.status(200).json(data);
          })
          .catch((err) => res.status(500).json({ dbError: "db error" }));
      }
    })
    .catch((error) => {
      res.status(500).json({ ERROR: error });
    });
};

const allExpertRoles = async (req, res) => {
  try {
    console.log("HERE!!!!!!");
    const { account_id } = req.params;
    const data = await db("expert_roles")
      .join("expert", "expert_roles.expert_id", "=", "expert.expert_id")
      .join("roles", "expert_roles.role_id", "=", "roles.role_id")
      .select(
        "expert_roles.expert_roles_id",
        "roles.role_name",
        "expert.first_name",
        "expert.last_name"
      )
      .where("expert_roles.account_id", account_id);

    if (data.length > 0) {
      res.status(200).json(data);
    } else {
      res.status(404).json({ INFO: "NO DATA FOUND" });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
};

const updateExpertRole = () => {
  const { account_id, expert_roles_id, expert_id, role_id } = req.body;
  db("expert_roles")
    .select({ expert_roles_id })
    .where(`account_id`, account_id)
    .where(`expert_roles_id`, expert_roles_id)
    .then((data) => {
      if (data.length > 0) {
        db("expert_roles")
          .update({
            expert_id,
            role_id,
          })
          .where(`expert_roles_id`, expert_roles_id)
          .returning("*")
          .then((data) => {
            res.json(data);
          })
          .catch((err) => res.status(400).json({ dbError: "db error" }));
      } else {
        res.json("data not found");
      }
    })
    .catch((err) => res.status(400).json({ dbError: "bad request" }));
};

// Assign Permissions to role
const assignPermissionToRole = (req, res) => {
  const { account_id } = req.params;
  const { permissions_id, role_id, created_on } = req.body;
  db("permissions_roles")
    .insert({ permissions_id, role_id, account_id, created_on })
    .returning("*")
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putRole = (req, res) => {
  const { role_id, role_name, account_id } = req.body;
  db("roles")
    .select({ role_id })
    .where("role_id", role_id)
    .where("account_id", account_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      const updated_at = new Date();
      if (data.length > 0) {
        db("roles")
          .update({
            role_name,
            updated_at,
          })
          .where("role_id", role_id)
          .where("account_id", account_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
      4;
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putPermission = (req, res) => {
  const { permission_id, permission, description, account_id } = req.body;
  db("roles")
    .select({ permission_id })
    .where("permission_id", permission_id)
    .where("account_id", account_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      const updated_at = new Date();
      if (data.length > 0) {
        db("roles")
          .update({
            permission,
            description,
            updated_at,
          })
          .where("permission_id", permission_id)
          .where("account_id", account_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getAllRoles,
  postAddRole,
  getAllPermissions,
  postAddPermission,
  assignRoleToExpert,
  assignPermissionToRole,
  putRole,
  putPermission,
  allExpertRoles,
  updateExpertRole,
};
