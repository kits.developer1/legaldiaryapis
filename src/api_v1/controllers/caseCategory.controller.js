const db = require("../../../data/db");

const getCaseCategory = (req, res) => {
  console.log(req.params);
  const { account_id } = req.params;
  console.log(account_id);
  db("case_category")
    .select("*")
    .where("account_id", account_id)
    .then((data) => {
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).json({ INFO: "NO DATA FOUND" });
      }
    })
    .catch((err) => {
      res.status(400).json({
        dbError: err,
      });
    });
};

const postCaseCategory = (req, res) => {
  const { account_id } = req.params;
  const { category, created_on } = req.body;
  db("case_category")
    .insert({ category, account_id, created_on })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putCaseCategory = (req, res) => {
  const { account_id, category_id, category } = req.body;
  db("case_category")
    .select({ category_id })
    .where("account_id", account_id)
    .where(`category_id`, category_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("case_category")
          .update({ category })
          .where("category_id", category_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const deleteCaseCategory = (req, res) => {
  const { account_id } = req.params;
  const { category_id } = req.body;
  db("case_category")
    .select({ category_id })
    .where("account_id", account_id)
    .where("category_id", category_id)
    .then((data) => {
      if (data) {
        db("case_category")
          .update({ is_delete: true })
          .where("category_id", category_id)
          .returning("*")
          .then((data) => {
            console.log(data);
            res.json(data);
          })
          .catch((err) => res.status(400).json({ dbError: "bad request" }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getCaseCategory,
  postCaseCategory,
  putCaseCategory,
  deleteCaseCategory,
};
