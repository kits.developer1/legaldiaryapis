const bcrypt = require("bcrypt-nodejs");
const db = require("../../../data/db");
require("dotenv").config();

const getProfile = (req, res) => {
  console.log(req);
  const account_id = req.user.data[0].account_id;
  const email = req.user.data[0].email;
  db.select("first_name", "last_name", "hash", "address")
    .from("expert")
    .where("account_id", account_id)
    .where("email", email)
    .then((data) => {
      console.log(data);
      if (data && data.length > 0) {
        res.status(200).json(data);
      } else {
        res.status(404).json({ Data: "No Data Found" });
      }
    })
    .catch((err) =>
      res.status(500).json({
        dbError: err,
      })
    );
};

const getExpert = (req, res) => {
  console.log(req.user);
  const email = req.user.expert;
  db.select("*")
    .from("expert")
    .where("email", email)
    .then((data) => {
      account_id = data[0].account_id;
      db.select("*")
        .from("expert")
        .where("account_id", "=", account_id)
        .where("is_delete", "=", false)
        .then((allExperts) => {
          console.log(allExperts);
          res.status(200).json(allExperts);
        });
    })
    .catch((err) =>
      res.status(500).json({
        dbError: err,
      })
    );
};

const postExpert = (req, res) => {
  console.log(req.body);
  const {
    account_id,
    first_name,
    last_name,
    password,
    gender,
    email,
    phone,
    address,
    created_on,
  } = req.body;
  const hash = bcrypt.hashSync(password);
  db("expert")
    .insert({
      account_id,
      first_name,
      last_name,
      hash,
      gender,
      email,
      phone,
      address,
      created_on,
    })
    .returning("*")
    .then((expertData) => {
      res.json(expertData);
    })
    .catch((err) => res.status(500).json({ dbError: err }));
};

const putExpert = (req, res) => {
  const {
    expert_id,
    first_name,
    last_name,
    password,
    gender,
    email,
    phone,
    address,
    account_id,
  } = req.body;
  const hash = bcrypt.hashSync(password);
  db("expert")
    .select({ expert_id })
    .where("expert_id", expert_id)
    .where("account_id", account_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("expert")
          .update({
            first_name,
            last_name,
            hash,
            gender,
            email,
            phone,
            address,
          })
          .where("expert_id", expert_id)
          .where("account_id", account_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putAccountExpertProfile = (req, res) => {
  const {
    first_name,
    last_name,
    password,
    gender,
    email,
    address,
    account_id,
  } = req.body;
  const hash = bcrypt.hashSync(password);
  db.select("email")
    .from("expert")
    .where("email", email)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("expert")
          .update({
            first_name,
            last_name,
            hash,
            gender,
            address,
            account_id,
          })
          .where("email", email)
          .returning("*")
          .then((update) => {
            res.status(200).json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "upate Error" }));
      } else {
        res.status(404).json("PROFILE NOT FOUND");
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

const deleteExpert = (req, res) => {
  const { account_id } = req.params;
  const { expert_id } = req.body;
  db("expert")
    .select({ expert_id })
    .where("expert_id", expert_id)
    .where("account_id", account_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        console.log(data.expert_id);
        db("expert")
          .update({ is_delete: true })
          .where("expert_id", expert_id)
          .where("account_id", account_id)
          .returning("*")
          .then((deleteUser) => {
            res.json(deleteUser);
          })
          .catch((err) => res.status(500).json({ dbError: err }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: err }));
};

const blockExpert = (req, res) => {
  const { account_id } = req.params;
  const { expert_id } = req.body;
  db("expert")
    .select({ expert_id })
    .where("expert_id", expert_id)
    .where("account_id", account_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        console.log(data.expert_id);
        db("expert")
          .update({ is_block: true })
          .where("expert_id", expert_id)
          .where("account_id", account_id)
          .returning("*")
          .then((blockUser) => {
            res.json(blockUser);
          })
          .catch((err) => res.status(500).json({ dbError: err }));
      } else {
        res.status(404).json("block failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: err }));
};

const unBlockExpert = (req, res) => {
  console.log("HERE!!!");
  console.log(req.body);
  console.log("HERE!!!");
  const { account_id } = req.params;
  const { expert_id } = req.body;
  db("expert")
    .select({ expert_id })
    .where("expert_id", expert_id)
    .where("account_id", account_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        console.log(data.expert_id);
        db("expert")
          .update({ is_block: false })
          .where("expert_id", expert_id)
          .where("account_id", account_id)
          .returning("*")
          .then((blockUser) => {
            res.json(blockUser);
          })
          .catch((err) => res.status(500).json({ dbError: err }));
      } else {
        res.status(404).json("unBlock failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: err }));
};

const totalNumberOfExperts = (req, res) => {
  const { account_id } = req.params;
  db("expert")
    .count("expert_id")
    .where("account_id", account_id)
    .where("is_delete", "=", false)
    .then((total) => {
      res.json(total);
    })
    .catch((err) => res.status(400).json({ Error: "bad request" }));
};

module.exports = {
  getExpert,
  postExpert,
  putExpert,
  deleteExpert,
  blockExpert,
  unBlockExpert,
  getProfile,
  putAccountExpertProfile,
  totalNumberOfExperts,
};
