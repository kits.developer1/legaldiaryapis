const db = require("../../../data/db");
require("dotenv").config();

const getAccountExpertInfo = (req, res) => {
  const { account_id } = req.params;
  db("accounts")
    .join("expert", "accounts.account_id", "=", "expert.account_id")
    .select(
      "accounts.law_firm",
      "accounts.account_name",
      "expert.first_name",
      "expert.last_name",
      "expert.email"
    )
    .where("account_id", account_id)
    .then((data) => {
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).json({ INFO: "NO DATA FOUND" });
      }
    })
    .catch((err) => {
      res.status(400).json({
        dbError: err,
      });
    });
};

module.exports = {
  getAccountExpertInfo,
};
