const { sign } = require("jsonwebtoken");

const createAccessToken = (expert) => {
  return sign({ expert }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "5d",
  });
};

const createRefreshToken = (expert) => {
  return sign({ expert }, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: "7d",
  });
};

const sendAccessToken = (res, expertData, userRoles, accessToken) => {
  var user = {
    accessToken: accessToken,
    expert: expertData,
    userRoles: userRoles,
  };
  return res.status(200).json({ user });
};

const sendRefreshToken = (res, refreshToken) => {
  res.cookie("refreshToken", refreshToken, {
    httpOnly: true,
    secure: true,
    sameSite: "lax",
    path: "/",
  });
};
module.exports = {
  createAccessToken,
  createRefreshToken,
  sendAccessToken,
  sendRefreshToken,
};
