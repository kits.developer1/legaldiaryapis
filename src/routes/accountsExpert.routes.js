/**
 * File: src/route/client.js
 * Description: account routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const getAccountExpertInfoController = require("../api_v1/controllers/account_expert.controller");
const fileUpload = require("../api_v1/middlewares/fileUpload");

// ==> Defining routes for  - 'accounts';

// ==> Route responsible for adding a new 'account':(POST ) localhost: 3000/api/account;
router.get(
  "/AccountExpertData",
  getAccountExpertInfoController.getAccountExpertInfo
);

module.exports = router;
