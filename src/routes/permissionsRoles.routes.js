/**
 * File: src/route/permissionRoles.js
 * Description: case category routes for Kutora
 * Date: 23/10/2023
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const permissionsRolesController = require("../api_v1/controllers/permissions_roles.controller");
const verifyToken = require("../api_v1/middlewares/authentication");

// ==> Defining routes for CRUD - 'Permissions and Roles';

// ==> Route responsible for Permissions and Roles CRUD;
router.get("/AllRoles/:account_id", permissionsRolesController.getAllRoles);
router.post("/AddRole/:account_id", permissionsRolesController.postAddRole);
// router.put("/UpdateCaseCategory", caseCategoryController.putCaseCategory);
// router.delete("/DeleteCaseCategory", caseCategoryController.deleteCaseCategory);

// ===> ASSIGN ROLE TO EXPERT ROUTE
router.post(
  "/AssignRole/:account_id",
  permissionsRolesController.assignRoleToExpert
);

// ===> ASSIGN PERMISSIONS TO ROLES ROUTE
router.post(
  "/AssignPermission/:account_id",
  permissionsRolesController.assignPermissionToRole
);

router.get(
  "/AllExpertRoles/:account_id",
  permissionsRolesController.allExpertRoles
);

router.put(
  "/UpdateExpertRole",
  verifyToken,
  permissionsRolesController.updateExpertRole
);

// ==> Permissions
router.get(
  "/AllPermissions/:account_id",
  permissionsRolesController.getAllPermissions
);
router.post(
  "/AddPermission/:account_id",
  permissionsRolesController.postAddPermission
);

router.put("/UpdateRole", permissionsRolesController.putRole);
router.put(
  "/UpdatePermission",
  verifyToken,
  permissionsRolesController.putPermission
);

module.exports = router;
