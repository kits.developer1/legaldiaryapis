/**
 * File: src/route/client.js
 * Description: case routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const caseController = require("../api_v1/controllers/case.controller");

// ==> Defining routes for CRUD - 'case';

// ==> Route responsible for adding a new 'case':(POST ) localhost: 3000/api/case;
router.get("/AllCases/:account", caseController.getCase);
router.post("/AddCase/:account", caseController.postCase);
router.put("/UpdateCase", caseController.putCase);
router.delete("/DeleteCase/:account", caseController.deleteCase);

module.exports = router;
