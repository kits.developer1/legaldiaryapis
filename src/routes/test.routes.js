/**
 * File: src/route/test.routes.js
 * Description:  tasks routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();

router.get("/api", (req, res) => {
  res.status(200).send({
    success: "true",
    message: "Ligal API Node.js + PostgreSQL + Express!",
    version: "1.0.0",
  });
});

module.exports = router;
