/**
 * File: src/route/client.js
 * Description: client routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const expertController = require("../api_v1/controllers/experts.controller");
const verifyToken = require("../api_v1/middlewares/authentication");

// ==> Defining routes for CRUD - 'clients';

// ==> Route responsible for adding a new 'client':(POST ) localhost: 3000/api/account;
router.get("/AllExperts", verifyToken, expertController.getExpert);
router.get("/Profile", verifyToken, expertController.getProfile);
router.post("/AddExpert", verifyToken, expertController.postExpert);
router.put("/UpdateExpert", verifyToken, expertController.putExpert);
router.get(
  "/TotalNumberOfExperts/:account_id",
  expertController.totalNumberOfExperts
);
router.put(
  "/UpdateAccountExpertProfile",
  verifyToken,
  expertController.putAccountExpertProfile
);
router.put("/DeleteExpert/:account_id", expertController.deleteExpert);
router.put("/BlockExpert/:account_id", expertController.blockExpert);
router.put("/UnBlockExpert/:account_id", expertController.unBlockExpert);
module.exports = router;
