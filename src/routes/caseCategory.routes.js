/**
 * File: src/route/client.js
 * Description: case category routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const caseCategoryController = require("../api_v1/controllers/caseCategory.controller");

// ==> Defining routes for CRUD - 'category';

// ==> Route responsible for adding a new 'client':(POST ) localhost: 3000/api/caseCategory;
router.get(
  "/AllCaseCategories/:account_id",
  caseCategoryController.getCaseCategory
);
router.post(
  "/AddCaseCategory/:account_id",
  caseCategoryController.postCaseCategory
);
router.put("/UpdateCaseCategory", caseCategoryController.putCaseCategory);
router.delete(
  "/DeleteCaseCategory/:account_id",
  caseCategoryController.deleteCaseCategory
);

module.exports = router;
