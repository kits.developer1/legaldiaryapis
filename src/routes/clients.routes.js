/**
 * File: src/route/client.js
 * Description: client routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const clientController = require("../api_v1/controllers/clients.controller");

// ==> Defining routes for CRUD - 'clients';

// ==> Route responsible for adding a new 'client':(POST ) localhost: 3000/api/account;
router.get("/AllClients/:account_id", clientController.getClients);
router.get(
  "/TotalNumberOfClients/:account_id",
  clientController.totalNumberOfClients
);
router.post("/AddClient/:account_id", clientController.postClient);
router.put("/UpdateClient", clientController.putClient);
router.put("/DeleteClient/:account_id", clientController.deleteClient);

module.exports = router;
