/**
 * File: src/route/tasks.routes.js
 * Description:  tasks routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const taskCaseController = require("../api_v1/controllers/task.controller");

// ==> Defining routes for CRUD - 'settlement case';

// ==> Route responsible for adding a new 'settlement case':(POST ) localhost: 3000/api/settlemencase;
router.get("/AllTasks", taskCaseController.getTasks);
router.post("/AddTask", taskCaseController.postTask);
router.put("/UpdateTask", taskCaseController.putTask);
router.delete("/DeleteTask", taskCaseController.deleteTask);

module.exports = router;
