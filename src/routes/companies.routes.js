/**
 * File: src/route/client.js
 * Description: client routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const companiesController = require("../api_v1/controllers/companies.controller");

// ==> Defining routes for CRUD - 'companies';

// ==> Route responsible for adding a new 'company':(POST ) localhost: 3000/api/company;
router.get("AllCompanies", companiesController.getCompany);
router.post("/AddCompany", companiesController.postCompany);
router.put("/UpdateCompany", companiesController.putCompany);
router.delete("/DeleteCompany", companiesController.deleteCompany);

module.exports = router;
