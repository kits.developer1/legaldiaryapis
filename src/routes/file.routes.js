/**
 * File: src/route/file.routes.js
 * Description: files routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const fileController = require("../api_v1/controllers/files.controller");

// ==> Defining routes for CRUD - 'file';

// ==> Route responsible for adding a new 'file ':(POST ) localhost: 3000/api/file;
router.post("/uploadFile", fileController.postFile);

module.exports = router;
