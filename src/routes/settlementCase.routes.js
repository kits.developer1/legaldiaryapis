/**
 * File: src/route/settlement.routes.js
 * Description: settlement case routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const router = require("express-promise-router")();
const settlementCaseController = require("../api_v1/controllers/settlementCase.controller");

// ==> Defining routes for CRUD - 'settlement case';

// ==> Route responsible for adding a new 'settlement case':(POST ) localhost: 3000/api/settlemencase;
router.get("/AllSettlementCases", settlementCaseController.getSettlementCase);
router.post("/AddSettlementCase", settlementCaseController.postSettlementCase);
router.put("/UpdateSettlementCase", settlementCaseController.putSettlementCase);
router.put(
  "/DeleteSettlementCase",
  settlementCaseController.deleteSettlementCase
);

module.exports = router;
