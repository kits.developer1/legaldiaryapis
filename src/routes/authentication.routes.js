/**
 * File: src/route/client.js
 * Description: account routes for Kutora
 * Date: 23/10/2021
 * Author: Monyuy Divine Kongadzem
 */

const validInfo = require("../api_v1/middlewares/validateInoutInfo");
const router = require("express-promise-router")();
const authController = require("../api_v1/controllers/authentication.controller");

// ==> Defining routes for  - 'auth';

// ==> Route responsible for adding a new 'account':(POST ) localhost: 3000/api/auth;

router.post("/register", validInfo, authController.postUser);
router.post("/signin", validInfo, authController.postLogin);
router.post("/logout", authController.logout);
router.post("/refresh_token", authController.refreshToken);
router.put("/updateaccount", authController.updateAccount);

module.exports = router;
