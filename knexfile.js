// Update with your config settings.
// To run migration "knex migrate:latest"
require("dotenv").config();

module.exports = {
  development: {
    client: "pg",
    connection: {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      database: process.env.DB_DATABASE,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      // ssl: { rejectUnauthorized: false },
      //host: process.env.DATABASE_HOST,
      //port: 5432,
      //user: process.env.DATABASE_USER,
      //password: process.env.DATABASE_PW,
      //database: process.env.DATABASE_DB,
      // host: "localhost",
      // port: 5432,
      // database: "kutora",
      // user: "postgres",
      // password: "megathrone",
    },
    migrations: {
      directory: "./data/migrations",
    },
    seeds: { directory: "./data/seeds" },
  },

  production: {
    client: "pg",
    connection: {
      connectionString: process.env.DATABASE_URL,
      ssl: { rejectUnauthorized: false },
      //host: process.env.RDS_HOSTNAME,
      //user: process.env.RDS_USERNAME,
      // password: process.env.RDS_PASSWORD,
      // port: process.env.RDS_PORT
      //database: process.env.RDS_DB_NAME,
    },
    migrations: {
      directory: "./data/migrations",
    },
    seeds: { directory: "./data/seeds" },
  },

  testing: {
    client: "pg",
    connectionString: process.env.connection,
    ssl: {
      rejectUnauthorized: process.env.ssl,
    },
    migrations: {
      directory: "./data/migrations",
    },
    seeds: { directory: "./data/seeds" },
  },
};
