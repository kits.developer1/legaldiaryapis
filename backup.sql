Create database kutora;

create table accounts(
    account_id serial PRIMARY KEY,
    account_name VARCHAR(60) UNIQUE NOT NULL,
    email VARCHAR(120) UNIQUE NOT NULL,
    phone VARCHAR(20) UNIQUE NOT NULL,
    law_firm VARCHAR(120) UNIQUE NOT NULL,
    account_type boolean NOT NULL DEFAULT TRUE,
    validated boolean NOT NULL DEFAULT FALSE,
    is_delete boolean NOT NULL DEFAULT FALSE,
    created_on date NOT NULL DEFAULT current_timestamp,
    deleted_at timestamp,
    updated_at timestamp 
);


create table expert(
    expert_id SERIAL PRIMARY KEY,
    first_name VARCHAR(30),
    last_name VARCHAR(30),
    email VARCHAR(50) UNIQUE NOT NULL,
    phone VARCHAR(20) UNIQUE,
    hash VARCHAR,
    address VARCHAR,
    gender VARCHAR,
    created_on date NOT NULL DEFAULT current_timestamp,
    deleted_at timestamp,
    updated_at timestamp,
    account_id int,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
);

create table roles(
    role_id SERIAL PRIMARY KEY,
    role_name VARCHAR UNIQUE,
    created_on date NOT NULL DEFAULT current_timestamp,
    deleted_at timestamp,
    updated_at timestamp,
    account_id int,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
);


create table expert_roles(
    expert_roles_id SERIAL PRIMARY KEY,
    expert_id INT NOT NULL,
    role_id INT NOT NULL,
    created_on date NOT NULL DEFAULT current_timestamp,
    deleted_at timestamp,
    updated_at timestamp,
    account_id INT NOT NULL,
    FOREIGN KEY (expert_id) REFERENCES expert(expert_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES roles(role_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
);

create table permissions(
    permission_id serial primary key ,
    permission VARCHAR UNIQUE,
    description text,
    active boolean,
    created_on date NOT NULL DEFAULT current_timestamp,
    deleted_at timestamp,
    updated_at timestamp,
    account_id int,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
);

create table permissions_roles(
    permissions_roles_id SERIAL PRIMARY KEY,
    permissions_id INT NOT NULL,
    roles_id INT NOT NULL,
    created_on date NOT NULL DEFAULT current_timestamp,
    deleted_at timestamp,
    updated_at timestamp,
    account_id INT NOT NULL,
    FOREIGN KEY (permissions_id) REFERENCES permissions(permission_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (roles_id) REFERENCES roles(role_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON DELETE CASCADE
);

create table case_category(
    category_id SERIAL PRIMARY KEY,
    category varchar unique,
    account_id int NOT NULL,
    created_on date NOT NULL default CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON UPDATE CASCADE ON DELETE CASCADE
);

create table clients(
    client_id SERIAL PRIMARY KEY,
    first_name VARCHAR(200) NOT NULL,
	last_name VARCHAR(200) NOT NULL,
    email VARCHAR(50) UNIQUE,
    profession VARCHAR(100),
    employer VARCHAR,
    phone VARCHAR UNIQUE,
    address VARCHAR,
    id_card_number VARCHAR NOT NULL,
    city VARCHAR,
    country VARCHAR,
    other_details VARCHAR,
    account_id INT NOT NULL,
    created_on date NOT NULL default CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (account_id) REFERENCES accounts(account_id) ON UPDATE CASCADE ON DELETE CASCADE
);

create table case_details(
    case_detail_id SERIAL PRIMARY KEY,
    case_ref_number VARCHAR UNIQUE NOT NULL,
    category_id INT ,
    judge VARCHAR,
    court_type VARCHAR,
    court_registrar VARCHAR,
    case_description text,
    client INT,
    expert INT,
    account INT,
    location VARCHAR,
    case_name VARCHAR,
    case_fee VARCHAR,
    non_client VARCHAR,
    court VARCHAR,
    open_date date,
    ajourned_date date,
    close_date date,
    status VARCHAR CHECK (status IN ('Open', 'Ajourned','Closed')),
    remark VARCHAR,
    created_on date NOT NULL default CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (category_id) REFERENCES case_category(category_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (expert) REFERENCES expert(expert_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (client) REFERENCES clients(client_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (account) REFERENCES accounts(account_id)  ON UPDATE CASCADE ON DELETE CASCADE
    );

create table case_expert(
    case_expert_id SERIAL PRIMARY KEY,
    case_detail_id INT ,
    expert_id INT,
    created_on date NOT NULL default CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (case_detail_id) REFERENCES case_details(case_detail_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (expert_id) REFERENCES expert(expert_id) ON UPDATE CASCADE ON DELETE CASCADE
);

