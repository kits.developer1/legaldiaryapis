const postAccount = (req, res, db) => {
  console.log("ok" + req);
  const { account_name, created_on } = req.body;
  db("accounts")
    .insert({ account_name, created_on })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(400).json({ dbError: "db error" }));
};

const getAccount = (req, res, db) => {
  const { account_id } = req.body;
  db("accounts")
    .select("account_logo")
    .where(`account_id`, account_id)
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(400).json({ dbError: "db error" }));
};

module.exports = {
  postAccount,
  getAccount,
};
