const getExpert = (req, res, db) => {
  const { account } = req.body;
  db.select("*")
    .from("expert")
    .where("account", account)
    .where("is_delete", false)
    .then((data) => {
      res.json(data);
    })
    .catch((err) =>
      res.status(500).json({
        dbError: "db error",
      })
    );
};

const postExpert = (req, res, db) => {
  const {
    first_name,
    middle_name,
    last_name,
    username,
    password,
    date_of_birth,
    gender,
    other_details,
    email,
    title,
    account,
    created_on,
    phone,
  } = req.body;

  db("expert")
    .insert({
      first_name,
      middle_name,
      last_name,
      username,
      password,
      date_of_birth,
      gender,
      other_details,
      email,
      title,
      account,
      created_on,
      phone,
    })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putExpert = (req, res, db) => {
  const {
    expert_id,
    first_name,
    middle_name,
    last_name,
    date_of_birth,
    gender,
    other_details,
    email,
    title,
    phone,
  } = req.body;
  db("expert")
    .select({ expert_id })
    .where("expert_id", expert_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("expert")
          .update({
            first_name,
            middle_name,
            last_name,
            date_of_birth,
            gender,
            other_details,
            email,
            title,
            phone,
          })
          .where("expert_id", expert_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const deleteExpert = (req, res, db) => {
  const { expert_id } = req.body;
  db("expert")
    .select({ expert_id })
    .where("expert_id", expert_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        console.log(data.expert_id);
        db("expert")
          .update({ is_delete: true })
          .where("expert_id", expert_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getExpert,
  postExpert,
  putExpert,
  deleteExpert,
};
