const getCaseCategory = (req,res,db)=>{
    db.select('*').from('case_category').where('is_delete',false)
    .then(data=>{
        res.json(data)
    })
    .catch(err=>res.status(500).json({
        dbError:'db error'
    }))
}



const postCaseCategory = (req,res,db)=>{
    const{name} = req.body;
     db('case_category').insert({name})
     .returning('*')
     .then(data=>{
         res.json(data)
     })
     .catch(err=>res.status(500).json({dbError:'db error'}))

}

const putCaseCategory = (req,res,db)=>{
    const{case_category_id,name} = req.body;
    db('case_category').select({case_category_id}).where(`case_category_id`,case_category_id)
    .then(data=>{
        // check if data exist, if exist update data using id
        if(data.length>0){
            db('case_category').update({name}).where('case_category_id',case_category_id)
            .returning('*')
            .then(update=>{
                res.json(update)
            })
            .catch(err=>res.status(500).json({dbError:'db errors'}))
        } else{
            res.json('update failed')
        }
        
    })
    .catch(err=>res.status(500).json({dbError:'db error'}))
}

 const deleteCaseCategory = (req,res,db)=>{
    const{case_category_id,name} = req.body;
    db('case_category').select({case_category_id}).where('case_category_id',case_category_id)
    .then(data=>{ 
        if(data){
           db('case_category').update({is_delete:true}).where('case_category_id', case_category_id)
            .returning('*')
           .then(data=>{
                console.log(data)
               res.json(data)
          })
           .catch(err=>res.status(400).json({dbError:'bad request'}))
        }
 else{
            res.json('delete failed')
       }
        
    })
    .catch(err=>res.status(500).json({dbError:'db error'}))
}


module.exports={
    getCaseCategory,
    postCaseCategory,
    putCaseCategory,
    deleteCaseCategory
}
