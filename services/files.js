const getFile = (res,req,db)=>{
    db.select('*').from('files')
    .then(data=>{
        res.json(data)
    })
    .catch(err=>res.status(500).json({
        dbError:'db error'
    }))

}
 
// FILE UPLOAD

const postFile = (req,res,db)=>{
    const{file_name,case_details,treated,remarks,filepath,key}=req.body;
    console.log(req.body)
    db('files').insert({file_name:file_name,case_details:case_details,treated:treated,remarks:remarks,filepath:filepath,created_on:new Date,key:key})
    .returning('*')
     .then(data=>{
         res.json(data)
     })
     .catch(err=>res.status(500).json({dbError:'db error'}))
   
}

// const postCompany = (req,res,db)=>{
//     const{company_name,reg_number,address,phone,fax,other_details,country,city,postal_code,created_on} = req.body;
//      db('company').insert({company_name,reg_number,address,phone,fax,other_details,country,city,postal_code,created_on})
//      .returning('*')
//      .then(data=>{
//          res.json(data)
//      })
//      .catch(err=>res.status(500).json({dbError:'db error'}))

// }



module.exports={
    getFile,
    postFile,
   
}