const getClient = (req, res, db) => {
  const { account } = req.body;
  db.select("*")
    .from("client")
    .where("account", account)
    .where("is_delete", false)
    .then((data) => {
      res.json(data);
    })
    .catch((err) =>
      res.status(400).json({
        dbError: "db error",
      })
    );
};

const postClient = (req, res, db) => {
  const {
    first_name,
    middle_name,
    last_name,
    date_of_birth,
    gender,
    other_details,
    company,
    title,
    email,
    country,
    mobile,
    fax,
    city,
    address,
    id_card_number,
    postal_code,
    account,
    created_on,
  } = req.body;
  db("client")
    .insert({
      first_name,
      middle_name,
      last_name,
      date_of_birth,
      gender,
      other_details,
      company,
      title,
      email,
      country,
      mobile,
      fax,
      city,
      address,
      id_card_number,
      postal_code,
      account,
      created_on,
    })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putClient = (req, res, db) => {
  const {
    client_id,
    first_name,
    middle_name,
    last_name,
    date_of_birth,
    gender,
    other_details,
    company,
    title,
    email,
    country,
    mobile,
    fax,
    city,
    address,
    postal_code,
  } = req.body;
  db("client")
    .select({ client_id })
    .where(`client_id`, client_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("client")
          .update({
            first_name,
            middle_name,
            last_name,
            date_of_birth,
            gender,
            other_details,
            company,
            title,
            email,
            country,
            mobile,
            fax,
            city,
            address,
            postal_code,
          })
          .where("client_id", client_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const deleteClient = (req, res, db) => {
  const {
    client_id,
    first_name,
    middle_name,
    last_name,
    date_of_birth,
    gender,
    other_details,
    company,
    title,
    email,
    country,
    mobile,
    fax,
    city,
    address,
    postal_code,
  } = req.body;
  db("client")
    .select({ client_id })
    .where("client_id", client_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data) {
        console.log(data.client_id);
        db("client")
          .update({ is_delete: true })
          .where("client_id", client_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("delete failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getClient,
  postClient,
  putClient,
  deleteClient,
};
