const getTasks = (req, res, db) => {
  db.select("*")
    .from("tasks")
    .then((data) => {
      res.json(data);
    })
    .catch((err) =>
      res.status(500).json({
        dbError: "db error",
      })
    );
};

const postTask = (req, res, db) => {
  const {
    subject,
    due_date,
    status,
    description,
    repeat,
    client,
    case_details,
    expert,
    created_on,
  } = req.body;
  db("Tasks")
    .insert({
      subject,
      due_date,
      status,
      description,
      repeat,
      client,
      case_details,
      expert,
      created_on,
    })
    .returning("*")
    .then((data) => {
      res.json(data);
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const putTask = (req, res, db) => {
  const {
    task_id,
    subject,
    due_date,
    status,
    description,
    repeat,
    client,
    case_details,
    expert,
  } = req.body;
  db("tasks")
    .select({ task_id })
    .where(`task_id`, task_id)
    .then((data) => {
      // check if data exist, if exist update data using id
      if (data.length > 0) {
        db("tasks")
          .update({
            subject,
            due_date,
            status,
            description,
            repeat,
            client,
            case_details,
            expert,
          })
          .where("task_id", task_id)
          .returning("*")
          .then((update) => {
            res.json(update);
          })
          .catch((err) => res.status(500).json({ dbError: "db errors" }));
      } else {
        res.json("update failed");
      }
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

const deleteTask = (req, res, db) => {
  const { task_id } = req.body;
  db("task")
    .select({ task_id })
    .where("task_id", task_id)
    .del()
    .then(() => {
      res.json({ delete: "true" });
    })
    .catch((err) => res.status(500).json({ dbError: "db error" }));
};

module.exports = {
  getTasks,
  postTask,
  putTask,
  deleteTask,
};
